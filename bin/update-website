#!/bin/bash
# © 2004 Joachim Breitner <nomeata@debian.org>
# changed by gregor herrmann <gregor+debian@comodo.priv.at>
# © 2011 Damyan Ivanov <dmn@debian.org>
# © 2018 Alex Muntada <alexm@debian.org>

# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

DIR=$(dirname $0)

for file in *.pod howto/*.pod
do
  html=$(echo "$file" | sed 's/\.pod$/.html/')
  $DIR/pod-simple-html "$file" > "$html"
done

#
# Build simple directory listings
#
TOP=$(pwd)
for dir in . $(find -type d); do
  pushd $TOP/$dir >/dev/null

  # Do not override an existing index.html
  [ -f index.html ] && continue

  # Mimic Apache's HeaderName in .htaccess
  if [ -f HEADER.html ]; then
    cp HEADER.html index.html
  else
    cp /dev/null index.html
  fi

  echo "<hr><h1>Directory Contents</h1>" >> index.html
  [ "$dir" != "." ] && echo '<a href="..">../</a><br>' >> index.html
  for D in $(find -maxdepth 1 -type d -name "[a-z]*" | sed -e 's,./,,' | sort); do
    [ "$D" = "bin" ] && continue
    echo "<a href=\"$D\">$D/</a><br>" >> index.html
  done
  for F in $(find -maxdepth 1 -type f -regextype egrep -regex '.*\.(ics|pdf|html)' | sed -e 's,./,,' | sort); do
    [ "$F" = "index.html" ] && continue
    [ "$F" = "HEADER.html" ] && continue
    echo "- <a href=\"$F\">$F</a><br>" >> index.html
  done
  echo "<hr>" >> index.html

  # Mimic Apache's ReadmeName in .htaccess
  if [ -f readme.txt ]; then
    echo "<pre>$(cat readme.txt)</pre>" >> index.html
  fi

  popd >/dev/null
done
