Debian Perl Group Rolling Sprint at DebCamp 2016
================================================

Introduction
------------

The Debian Perl Group conducted a "Rolling Sprint" during DebCamp 2016 in
Cape Town from 2016-06-23 - 2016-07-01.

The term "Rolling Sprint" describes an event that extended over the whole
DebCamp week with the opportunity for people to "hop on" and "hop off" for
one or more days. This made it possible for persons who had more plans
for DebCamp than one single area of work to participate according to their
availability.

The structure of the "Rolling Sprint" was:

* A(n almost) daily coordination meeting after lunch, where people
  coordinate the work for this day;
* people working on their selected tasks over the day, alone, in pairs, in
  groups;
* a progress report at the next meeting to summarize and document the results. 

Altogether 4 people took part in one or more of the 4 coordination meetings
and/or work sessions: bremner, carnil, gregoa, intrigeri.

The participants would like to thank the [sponsors][] of DebConf16 and the
DebConf Team for making this sprint possible.

[sponsors]: https://debconf16.debconf.org/sponsors/

Tools
-----

* pkg-perl-tools:
    - add `update` option to `dpt-upstream-repo`; this is now used in our
    `.mrconfig`
    - `patchedit`: always set Last-Updated to mtime of patch
    - examples/check-build: update to work with adt-run and autopkgtest
* dh-make-perl: new release:
    - now a native package
    - split into 2 binary packages: dh-make-perl and libdebian-source-perl
    - several bug fixes

QA tasks across packages
------------------------

* Subscribe our Launchpad team, ~pkg-perl-maintainers, to all bugs
  concerning packages we maintain.
* Git repos cleanup (propose to remove packages from Git that were injected
  but never finished for upload).
* Drop Mouse deprecation from our [TODO][] list: it's actually still somewhat maintained,
  and lots of packages still use it.
* Go through our RC bugs and propose package removals:
  <https://lists.debian.org/debian-perl/2016/06/msg00038.html>
* NMU a cpuple of non-team-maintained dependencies with RC bugs (dh compat 4)
  to avoid testing autoremovals of our packages.
* Change repackaging framework from repack.{stub,local} to Files-Excluded
  for all packages except a couple special cases. Update [repacking.pod][]
  documentation as well.
* Change (even more) URLs in debian/upstream/metadata to use HTTPS.
* Checked -perl packages affected by the [GCC 6 transition]: everything
  seems fine or already tracked upstream. Fix [#816571].

[TODO]: https://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks
[repackaging.pod]: https://pkg-perl.alioth.debian.org/howto/repacking.html
[GCC 6 transition]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=ftbfs-gcc-6;users=debian-gcc@lists.debian.org
[#816571]: https://bugs.debian.org/816571

Packages / bugs / uploads
-------------------------

* Fix [#828181][] in libmojolicious-perl in git.
* Update libdatetime-timezone-perl to Olson db 2016e in unstable and stable.
* Review half a dozen new packages, upload one, set others to UNRELEASED.
* Review another half dozen new packages, upload most, ask one question.
* Prepare dependencies for new libmessage-passing-zeromq-perl.
* Debug libgtk2-perl FTBFS on powerpc.
* New libio-socket-ssl-perl upload to unstable.
* File and fix [#829066][], [#829064][], thanks [ci.debian.net][CI].
* Debug libdevel-gdb-perl brittle test a bit ([#784845][]).
* Fixed a few FTBFS on the reproducible builds infrastructure:
  libnet-route-perl, ...

[#828181]: https://bugs.debian.org/828181
[#829066]: https://bugs.debian.org/829066
[#829064]: https://bugs.debian.org/829064
[CI]: https://ci.debian.net
[#784845]: https://bugs.debian.org/784845

Reproducible builds
-------------------

Made these packages build reproducibly:

* libgoo-canvas-perl
* libgtk2-perl
* libgnome2-perl
* libmarpa-r2-perl
* [#828635][] in libnet-tclink-perl
* [#828636][] in libembperl-perl
* latexdiff ([#814019][])
* libnanomsg-raw-perl

As of 2016-07-01, we're down to a mere 7 team-maintained packages not building
reproducibly.

[#828635]: https://bugs.debian.org/828635
[#828636]: https://bugs.debian.org/828636
[#814019]: https://bugs.debian.org/814019

Resources
=========

* Announcement:
  [Wiki](https://wiki.debian.org/Sprints/2016/DebianPerlDebCamp),
  [email](https://lists.debian.org/debian-perl/2016/06/msg00018.html)
* Notes:
  [Gobby](infinote://gobby.debian.org/Teams/Perl/Team-Sprint-Debcamp-2016)
  [GobbyWeb](https://gobby.debian.org/export/Teams/Perl/Team-Sprint-Debcamp-2016)
