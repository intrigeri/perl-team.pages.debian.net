% Debian Perl Rolling Sprint at DebCamp 2019

## Introduction

The Debian Perl Group conducted a "Rolling Sprint" during DebCamp 2019
in Curitiba, Brasil from July 16th to July 19th.

The term "Rolling Sprint" describes an event that extended over the whole
DebCamp week with the opportunity for people to "hop on" and "hop off" for
one or more days. This made it possible for persons who had more plans
for DebCamp than one single area of work to participate according to their
availability.

The structure of the "Rolling Sprint" was:

  * A daily coordination meeting at 14:30 (local time), where people
  coordinate the work until the next meeting;
  * people working on their selected tasks over the day, alone, in pairs, in
  groups 

Altogether 7 people took part in one or more of the coordination meetings
and/or work sessions: axhn, bremner, gregoa, intrigeri, joenio, nodens,
utkarsh.

The participants would like to thank the [sponsors][] of DebConf19 and the
DebConf Team for making this sprint possible.

[sponsors]: https://debconf19.debconf.org/sponsors/

## New releases of our software

We have released dh-make-perl 0.106 and pkg-perl-tools 0.52.

## Tools

  * release of pkg-perl-tools 0.106
  * add --prune to git-fetch upstream-repo in dpt-{checkout,upstream}

## Bugs

  * reply to #932012
  * upload fix for #930052 (perl 5.30)
  * upload fix for #930054 (perl 5.30)
  * upload fix for #930055 (perl 5.30)
  * #925070 (ITA)
  * #925005 (ITA)
  * #925031 (ITA)
  * #925034 (ITA)
  * #925110 (ITA)
  * … and more


## Cross-package QA

  * Rename debian/NEWS.Debian to debian/NEWS to get it actually installed
  * remove `dh_compress -Xexamples` with debhelper 12

## Embedded modules in packages at build time

We tried to map the issue of packages using modules embedded in inc/ at build time,
to get an understanding of the matter, and see if we can fix it globally.

Turns out we have:

  * 388 using Module::Build, Module::Install
  * 74 using some kind of custom build system or extending existing one
  * 21 using a local copy of Alien::* or Test::Base (+ Spiffy)
  * 4 using inc::latest

From this, we decided it was fixable if we allowed some exception. A proposal
of policy change has been prepared.

## Candidates for removal

  * libreadonly-xs-perl (#932126)
  * filed RM bugs for a bunch of libgtk2-perl reverse-dependencies that were not shipped in Buster
  * made all remaining libgtk2-perl reverse-dependencies RC-buggy:
    * https://bugs.debian.org/912860
    * https://lists.debian.org/debian-devel/2018/11/msg00570.html
    * https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=gtk2-removal;users=debian-perl@lists.debian.org`

  Requests for removal:

  * [#932126](https://bugs.debian.org/932126) (libreadonly-xs-perl)

## New upstream releases

We have uploaded at least 52 new upstream releases:

  * libalt-alien-ffi-system-perl
  * libanyevent-perl
  * libapache-db-perl
  * libarchive-any-perl
  * libarchive-tar-wrapper-perl
  * libbson-perl
  * libbson-xs-perl
  * libcgi-fast-perl
  * libcgi-pm-perl
  * libclass-inspector-perl
  * libclipboard-perl
  * libcompress-raw-bzip2-perl
  * libcompress-raw-lzma-perl
  * libcompress-raw-zlib-perl
  * libcourriel-perl
  * libcpanel-json-xs-perl
  * libdate-manip-perl
  * libdevel-stacktrace-perl
  * libdist-zilla-plugin-git-perl
  * libdist-zilla-plugin-installguide-perl
  * libdist-zilla-plugin-makemaker-awesome-perl
  * libdist-zilla-plugin-ourpkgversion-perl
  * libffi-checklib-perl
  * libffi-platypus-perl
  * libfuture-asyncawait-perl
  * libgtk3-perl
  * libimage-exiftool-perl
  * libio-aio-perl
  * libio-async-perl
  * libio-compress-lzma-perl
  * libio-compress-perl
  * libio-socket-ssl-perl
  * liblwp-mediatypes-perl
  * libmoosex-role-parameterized-perl
  * libnet-amazon-s3-perl
  * libnet-appliance-session-perl
  * libnet-cli-interact-perl
  * libnet-http-perl
  * libnetpacket-perl
  * libnet-ssleay-perl
  * libpgplot-perl
  * libregexp-grammars-perl
  * libstrictures-perl
  * libsub-quote-perl
  * libsyntax-keyword-try-perl
  * libterm-choose-perl
  * libtest2-suite-perl
  * libtext-bibtex-perl
  * libtickit-perl
  * libwww-perl
  * libyaml-libyaml-perl
  * libyaml-perl


## New packages

We have uploaded at least 2 new packages:

  * libextutils-hascompiler-perl
  * libextutils-makemaker-dist-zilla-develop-perl
