Debian Perl Group Sprint @ DSC 2017
===================================

See https://wiki.debian.org/Sprints/2017/DebianPerlSprint

https://udd.debian.org/cgi-bin/bts-usertags.cgi?tag=bcn2017&user=debian-perl%40lists.debian.org
or
https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=bcn2017;users=debian-perl@lists.debian.org

bts user debian-perl@lists.debian.org . usertag nnnnnn + bcn2017

* ... done
+ ... todo

Bugs/packages
=============

+ perl 5.26.0-RC1 packaged and uploaded to experimental
  - switched to versioned Provides, see 'Discussions' below
  - started first test rebuilds, see the Perl-5.26-QA gobby document
  - updated the transition scripts in the pkg-perl scripts repo
  - upload fixed libparams-classify-perl to experimental, libwx-perl and libterm-readline-zoid-perl to unstable,
    libdevel-cover-perl_1.25 to experimental, libdate-pcalc-perl 6.1-5 to experimental
  - identified a probable stretch->buster upgrade issue with debconf (#863071, patch included) that may need fixing for stretch
+ We might want to go through https://udd.debian.org/cgi-bin/bts-usertags.cgi?tag=rm-candidate&user=debian-perl%40lists.debian.org and/or add bugs there
* #861952 / libcgi-validop-perl: removal candidate → RM bug filed: #863073
+ RC bugs in jessie: https://lists.alioth.debian.org/pipermail/pkg-perl-maintainers/2017-May/110256.html
  - look at all of them, prepare updates for 8, others are probably candidates for jessie-ignore or similar
* Fix libfile-sharedir-par-perl autopkgtest issues
  - used smoke-setup instead of smoke-files to build blib contents
* Takeover libterm-readline-zoid-perl
* pkg-perl-tools: Broken link for repack.sh in examples/repack.stub (#853045)
* pkg-perl-tools: "dpt gen-itp" prints Config::Model warning inside ITP text (#832059) 

Other tasks
===========

* Re-subscribe our Launchpad team, ~pkg-perl-maintainers to all bugs concerning packages we maintain.
* Fix spelling mistakes in d/changelog in some packages.
* First look into orphaned lib.*-perl packages referred by the MIA team
+ Look into orphaned packages
  - might be enough to look for "O:" packages in https://pkg-perl.alioth.debian.org/qa/wnpp.html
  - checked that the packages mentioned by the MIA team to debian-perl are already there
* Build the pkg-perl handbook with sphinx locally and rsync to alioth
  - building on alioth was discarded
  - details in website/README.sphinx
* Discuss the use of inc/ directories in pkg-perl packages
  - added a summary of the ideas discussed in OpenTasks wiki page
* Reviewed wiki.d.o Perl pages
  - Page "Perl/Downloads" was successfully deleted!
  - Renamed Perl5.10Transition to PerlMaintenance/Perl5.10Transition
  - Renamed Perl518TransitionBugs to PerlMaintenance/Perl518TransitionBugs
  - Added link to "Debian Perl Group founded" message in Teams/DebianPerlGroup
  - May be interesting to move the content of PerlBugSquashing to the pkg-perl website/handbook
  - Review pending for PerlMaintenance/Perl5MajorUpdateProcess (last update in 2013)
* Improved wording of directory chroot type in website/autopkgtest.pod
  - Thanks to Tincho for noticing!
  - Added explanation on how to add eatmydata support in schroot to speed up builds
* pkg-perl-autopkgtest: smoke test: run prove recursively
* upgraded perl.debian.net to a new more powerful host for the 5.26 rebuilds (Dom + ntyni)
  + done but CNAME not moved quite yet

Discussions/projects
====================

* Versioned Provides
  - should work in toolchain and infrastructure
    + vague memories about wanna-build issues? dose3, fixed in stretch (#786671)
    + britney support has been in place for a year (#786803)
  - src:perl can e.g. Provides: libtest-simple-perl (= 1.xxxxxx)
  - other packages can then only (Build-)Depends: libtest-simple-perl (>= 1.xxxxxx)
    without an alternative dependency on perl before or after
  - debian-policy bug: #761219
* upload to experimental (DONE)
  - ask on -devel, ask release team
  - change reverse dependencies (update cme probably) at their own speed
  - change/remove lintian check (versioned-dependency-satisfied-by-perl)
  - upload a couple packages using the new versioned provides to test

* walked through the pkg-perl-autopkgtest setup with Tincho
  - noted that the number of indirection levels is probably needlessly high since autodep8 came along
  - open question: is there any difference between Testsuite:autopkgtest and Testsuite:autopkgtest-pkg-perl nowadays?

* pushed a 'get-ci-failures' script to the pkg-perl scripts repo; this can be used to query ci.debian.net failures

* Insecure loading of YAML data
  - The feature must not be turned off globally as this will break several
    packages in Debian.
  - The switch to control the behaviour should be an environment variable
    since several packages have been written to use any of the available
    loader. There should be just one switch that works for all implementations.
  - test rebuilds on perl.debian.net with the blessing turned off (in a couple of different ways) revealed a dozen or so FTBFS regressions
  - Seems disable blessing is still way too invasive. You it will rather be
    checking for a DESTROY method and inhibit instatiation only then.

* Using Perl::Critic to find issues in Perl modules (2-arg open, etc.) as suggested by Paul Wise
  - 0. Run perlcritic (and maybe other linters) over new/updated Perl code introduced to Debian
       https://anonscm.debian.org/cgit/collab-maint/check-all-the-things.git/tree/data/perl
  - 1. make a report of modules affected by default perl-critic config
       with --noprofile to avoid running untrusted code
  - 2. tweak perl-critic config
  - 3. identify potential security issues and review code
       + use lib
       + eval
       + qx/``
       + system/sysopen/popen/open/open2/open3 without lists
       + open with pipes 
  - 4. If ever possible have a script to determine which Modules should be
       checked first.
  - 5. Talk with upstream about deprecating all these things.


