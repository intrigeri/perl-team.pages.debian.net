Debian Perl Sprint 2015
=======================

Introduction
------------

7 members of the Debian Perl Group met in Barcelona over the weekend
from May 22 to May 24 2015 to kick off the development around perl for
Stretch and to work on QA tasks across our 3000+ packages. The
preparation details can be found on the [sprint wiki][].

The participants would like to thank the Computer Architecture Dept.
of the Universitat Politècnica de Catalunya ([DAC UPC][]) for hosting
us, and all donors to the Debian project who helped to cover a large
part of our expenses.

[sprint wiki]: https://wiki.debian.org/Sprints/2015/DebianPerlSprint
[DAC UPC]: http://www.ac.upc.edu/


Bugs and Uploads
================

Overview
--------

Bugs [tagged][] with:

* user: debian-perl@lists.debian.org
* usertags: bcn2015

A total of 53 bugs were filed/worked on. These include:

* newly filed: 28 bugs
    - incl. 6 fixed during the sprint
    - incl. 8 RM bugs to drop RC-buggy unmaintained upstream software
    - incl. 6 bugs on DUCK after running it over all group's packages
* resolved: 13 bugs
    - incl. 6 bugs filed during the sprint

A total of 31 accepted uploads were made at the sprint.

[tagged]: https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=bcn2015;users=debian-perl@lists.debian.org


Upstream Bug Reports Filed
--------------------------

* [Config::Model::TkUi](https://github.com/dod38fr/config-model-tk-ui/issues/1)
  (already fixed)
* [Gtk3](https://mail.gnome.org/archives/gtk-perl-list/2015-May/msg00020.html)


FTBFS Bugs
----------

* 7 RM bugs filed for packages with FTBFS bugs since Perl 5.18
  (i.e. for 2 years).
* [OpenTasks wiki page][opentasks] updated

[opentasks]: https://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks

Git and Patches
===============

(Followup to [pkg-perl BoF at DebConf14][bof2014].)

The group discussed the current practice for patch management, and
possible alternatives.

Currently we are storing patches in git with quilt, with the actual
source in git unpatched.

This has downsides like having to remember `quilt push -a` when running
`debian/rules build` manually from a git checkout. Also, git would be a
better tool at rebasing/merging the patches to new upstream releases,
but currently this needs to be done manually with quilt.

The upside is that it's very simple and straightforward; we need to
consider also less deeply involved team members, and requiring
complicated workflows can drive them away.

[bof2014]: https://lists.debian.org/debian-perl/2014/08/msg00044.html

git-debcherry
-------------

One tool: git-debcherry (David Bremner), shipped in the gitpkg package.

Missing piece: preserving patch meta-information (DEP3).

* the commits changing upstream code don't get rebased in this workflow,
  but the metadata is volatile
* git notes solve this neatly: the metadata is attached to commits but
  can be modified later

git-notes is still a bit rough UI-wise:

* merging is not straightforward if several people work on the same
  notes at the same time (not expected to be a real problem)
* needs manual pushing/fetching, could probably be integrated into
  team-specific tools like dpt-push

Niko presented a [demo][] of the patched git-debcherry and proof of
concept conversion scripts:

* `git-debcherry-784159`

    git-debcherry script from gitpkg_0.26, enhanced to support git
    notes with the patch from [#784159][]

* `quilt2debcherry`

    script to convert quilt patches in a package into a commit series
    with the patch metadata stored as git notes

* `run-debcherry`

    script to automate exporting patches from the commit series and
    notes, by first running git-debcherry itself and then mangling the
    results aiming for stable output with the two scripts below

* `notes2dep3`

    script to convert `git format-patch` output of commits with patch
    metadata inside git notes into a DEP3-compatible format

* `rename-patches`

    script to rename patches as created by `git format-patch` into
    names specified by the *Patch-Name* header stored in the git notes

Conclusions wrt. git-debcherry: it needs a separate *export* step to
keep debian/patches synced with the actual git commits, and it's not
clear how that should fit in a standard workflow. Possibly this is
comparable to debian/changelog handling with tools like gbp-dch, and it
should be the responsibility of everybody who works on a package to
leave debian/patches in a *sane state* afterwards.

Storing patch metadata in git notes looks very interesting and may have
broader applications than just with git-debcherry.

There's still not much experience in how well git-debcherry works in the
long run.

Integration into git-buildpackage (`~/.gbp.conf`) by gregor:

```
[buildpackage]
prebuild = GIT_DIR=$GBP_GIT_DIR git-debcherry -o debian/patches upstream
```

Issues:

* where to export (and commit?) patches; in master, a separate release
  branch? (question for bremner)
* needs more thoughts, good documentation, ...

Actions:

* David to review git-debcherry patch.
* Looking at it again at the Rolling Sprint (DebCamp).

[demo]: http://anonscm.debian.org/cgit/pkg-perl/scripts.git/tree/debcherry
[#784159]: https://bugs.debian.org/784159


git-dpm
-------

Dominic demonstrated git-dpm (as used in src:perl). The main differences
between git-dpm and git-debcherry are:

* git-dpm has exported and applied debian/patches in master;
* git-debcherry needs export (so not always buildable from the git
  master branch).


Debian Perl Tools
=================

dpt (short for Debian Perl Tools) is a wrapper around a couple of helper
scripts useful for pkg-perl, and packaged in pkg-perl-tools.


Importing Upstream Git History
------------------------------

The group discussed whether upstream git history should be imported
into pkg-perl git repositories.

Some of us use `dpt import-orig`, `dpt upstream-repo` and
`dpt debian-upstream` to import upstream's Git history into our
*upstream* branch, when importing a new upstream release.

It is recommended to do that as well, especially for packages where
it has already been done in the past (to avoid messy history on the
*upstream* Git branch).


Pushing Upstream Git Tags to Alioth
-----------------------------------

Related to the above, the group discussed whether upstream tags
should be pushed. The current practice is to not do so, but it's easy
to accidentally do if `git push --tags` is used. However, `dpt push`
does not do it by default.

No change to the status quo is needed.


Maintaining debian/upstream/metadata
------------------------------------

The group reviewed current practice.

`dpt upstream-metadata` creates this file. It is usually only run once,
further changes must be done manually. **We have to keep an eye on such
updates when importing new upstream releases.**


Documenting More dpt Commands
-----------------------------

It was noted that the team's documentation ([git.html][]) needs to be
checked for new dpt commands (as discussed above). Damyan made small
corrections here and there.

[git.html]: http://pkg-perl.alioth.debian.org/git.html


Documentation Review
====================

* It was noted that many of the HTML docs on pkg-perl.alioth.debian.org
  should be reviewed.
* All ancient ones (everything not touched since 2011) were removed.
* The autopkgtest docs were improved, with some notes and a new section
  about setting up schroot by Alex.


src:perl Plans and Packaging Changes for Stretch
================================================

Niko presented [plans][] for reorganizing src:perl packaging to achieve
co-installability of libperl5.xx (including the standard library) for
multiple versions and architectures, and asked for feedback. The ensuing
discussion was good and constructive.

Preference for Option S (least packages, with the standard library
bundled into libperl5.xx), with `perl-modules-*`

* Option S most simple, but with some file duplication
* Option L most perfect (no duplication, etc.) but quite complex and
  maybe hard to maintain properly (lots of break/conflicts/replaces,
  etc, possible pre-depends fragility)
* The perl package maintainers (Niko and Dominic) prefer Option S due to
  its simplicity.

Actions:

* Niko and Dominic to move forward with Option S (with perl-modules-5.xx)
  with Perl 5.22
* upload to experimental (DONE), aimed for sid in a couple of months
* normal rebuild testing (already started), plus possibly re-running
  autopkgtest checks
* announce on -devel (TBD)
* change Perl policy where needed
    + [draft][] by Dominic
      (to file bug once upload to experimental is done)

[plans]: http://people.debian.org/~ntyni/perl/libperl/
[draft]: http://lists.alioth.debian.org/pipermail/perl-maintainers/2015-May/004889.html


Investigating Build Failures for perl on buildds
================================================

Bug [#785557][] seems to relate to the times(2) system call returning
zero for user CPU time. Some investigation work on this bug was done.

[#785557]: https://bugs.debian.org/785557


Upcoming perl 5.22 Release and Cleanup
======================================

Bugs on packages build-depending on perl5 (will be gone in 5.22) were
filed by Dominic:

* dh-lisp
* fte (DONE via QA upload)
* htag
* libconvert-units-perl (likely can be put under the pkg-perl umbrella
  as eloy was fine with similar cases)
* liblingua-en-words2nums-perl (DONE)
* libmasonx-request-withapachesession-perl (DONE)
* librtf-document-perl
* mimefilter

A test repository containing packages rebuilt again perl 5.22 was
prepared:

* [https://people.debian.org/~dom/perl/test/perl-5.22.0/setup.sh](https://people.debian.org/~dom/perl/test/perl-5.22.0/setup.sh)
* [https://people.debian.org/~dom/perl/test/perl-5.22.0/setup.sh.asc](https://people.debian.org/~dom/perl/test/perl-5.22.0/setup.sh.asc)


ci.debian.net
=============

CI and KGB
----------

A plan for producing IRC notifications on autopkgtest regressions
was discussed:

* Parse [CI JSON] every day and report any new failures
  (`previous_status=='pass'`) via KGB
* Only for pkg-perl packages (list retrieved via UDD)
* adt-kgb tool idea appeared, reusable by other teams. Then Damyan wrote
  `script/kgb-ci-report` in KGB. It is available so that e.g. other
  teams can re-use it.

[CI JSON]: http://ci.debian.net/data/status/unstable/amd64/packages.json

Newly autopkgtest-able Packages
-------------------------------

Some of those were already on the whitelist, some others were fixed so
that they can now be autopkgtest'ed.

* libb-hooks-op-annotation-perl
* libcairo-gobject-perl
* libcairo-perl
* libclutter-perl
* libdevice-cdio-perl
* libgtk3-perl
* libmoox-handlesvia-perl
* libmoox-late-perl
* libmoox-types-mooselike-perl
* libnet-dbus-perl
* libpango-perl
* libparse-debianchangelog-perl

Several other packages got fixed to make their autopkgtests work.


Whitelist Updates
-----------------

The status of ci.debian.org autopkgtest whitelisting was reviewed. Notes
from this work:

* ci.debian.net has a whitelist for perl packages which succeeded on
  initial manual run.
* Perl packages are tested via autopkgtest-pkg-perl even if they don't
  have a *Testsuite* header.
* [Cleanup][] of the ci.debian.net whitelist.
* Test run results of [non-whitelisted][] packages.
* Updated autopkgtest.pod with the results plus a list of
  [work needing][] packages.

[Cleanup]: http://anonscm.debian.org/cgit/collab-maint/debian-ci-config.git/commit/?id=b27313668c16bc77f3d11c027c4fb5fe902c772a
[non-whitelisted]: http://anonscm.debian.org/cgit/collab-maint/debian-ci-config.git/commit/?id=1421bfbcd1b62e0125bf323cdb33771734c9679a
[work needing]: http://anonscm.debian.org/cgit/pkg-perl/website.git/commit/?id=07a1031986e96974bb55f4d216f1476c9832bc43


Migrations
==========

Outstanding migratations involve team maintained packages were
reviewed:

* Gstreamer 0.10 -> 1.0 ([#785856][]):

    would need a new libgstreamer1-perl package; few people care, so have
    libgstreamer-perl removed and package libgstreamer1-perl later if
    somebody needs it.

* OGRE ([#732725][]):

    low popcon (max 26, now 22); no reverse deps (just one Recommends
    and one Enhances); the conclusion is that this is not really needed
    and seems dead upstream; let go.

* libois-perl possibly doesn't make much sense without libogre-perl.

[#785856]: https://bugs.debian.org/785856
[#732725]: https://bugs.debian.org/732725


Recurring Tasks
===============

Work was done on various recurring tasks done by the team:

* [Running DUCK][] over all our packages by Axel:
    + 6 bug reports filed against duck due to false positives. Already
      positive feedback from duck's maintainer.
    + Axel managed to go through all non-e-mail issues up to *libf* during
      the sprint and travelling back, continuing afterwards. Currently
      [still open][] issue.
* gregor (re)subscribed pkg-perl launchpad team to all our packages

[Running DUCK]: https://people.debian.org/~abe/pkg-perl-duck-2015-05-23.txt
[still open]: https://people.debian.org/~abe/pkg-perl-duck-2015-05-23.updated.txt


Bugs in Launchpad
=================

There is the pkg-perl-maintainers [launchpad team][] that people can
join and then get mails (and the [launchpad account][] for the
Debian Perl Group). The status of this arrangement was reviewed.

For the valid-also-for-Debian reports, forwarding to the BTS sounds like
the way to go; and/or close them with an upload to Debian (with the
`LP: #nn` syntax).

[launchpad team]: https://launchpad.net/~pkg-perl-maintainers
[launchpad account]: https://launchpad.net/~pkg-perl-maintainers-lists-alioth


Handling debian/changelog
=========================

After some discussion we discovered that the sensible way is already
described in our documentation as a [commit policy][]:

> The current group policy states that whenever you stop working on
> a package, the changelog should be updated and pushed.

[commit policy]: https://pkg-perl.alioth.debian.org/git.html#commit_policy


libfile-fcntllock-perl
======================

The problem mentioned in [#677865] was discussed; conclusions:

* File::FcntlLock (upstream?) needs to fall back to ::Pure if ::XS
  is not available
* the Debian package needs to install ::Pure into a non-version
  specific path as above
* `Depends: perlapi-*` can be demoted to Recommends
* dpkg-dev can Depend on libfile-fcntllock-perl
* newer versions of perl may want to Break older versions of
  libfile-fcntllock-perl, but what about the varying binNMU suffixes,
  etc.?
* [#677865][] updated, upstream cc'd with the feature request about this
  fallback

[#677865]: https://bugs.debian.org/677865


Reproducibility and `POD_MAN_DATE`
==================================

Niko commented on [#782879][] and [#782878][]; waiting for input from
debhelper maintainers.

[#782879]: https://bugs.debian.org/782879
[#782878]: https://bugs.debian.org/782878
