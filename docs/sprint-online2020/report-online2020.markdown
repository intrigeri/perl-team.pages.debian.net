Debian Perl Sprint 2020
=======================

Introduction
------------

Between 15-17 May 2020, 8 members of the Debian Perl team met virtually
for their 6th annual sprint. Rather than meeting in a European city,
as before, this sprint took place with remote participants,
using Jitsi and IRC for communication and collaboration.

Full details are available on the [sprint wiki page][] and [Gobby notes]
and some highlights are mentioned below.

[sprint wiki page]: https://wiki.debian.org/Sprints/2020/DebianPerlSprint
[Gobby notes]: https://gobby.debian.org/export/Teams/Perl/Team-Sprint-Online-2020

Highlights
==========

The pattern of daily coordination meetings, focussed discussion slots,
and collaborative working on IRC and the occasional breakout room
was maintained successfully, even though the team was not able to
socialise to the same degree as usual. Other demands on participants'
time also limited the amount of work that could be done during the
sprint.

Package removals
----------------

Progress was made on analysing and in some cases removing some team
maintained packages that had outlived their use, helping to reduce our
technical debt. A summary of still to process bugs is at

<https://udd.debian.org/cgi-bin/bts-usertags.cgi?tag=rm-candidate&user=debian-perl%40lists.debian.org>

Perl interpreter
----------------

A test build of perl 5.31 (which will become 5.32) was prepared
and test rebuilds of dependent packages begun. In addition,
the latest maintenance release, 5.30.2, was uploaded to unstable.
Both involved fixes to metaconfig and perl upstream, to ensure that
the metaconfig source is an accurate reflection of perl's Configure
script.

Assessing the test rebuild logs is ongoing at

<http://perl.debian.net/>.

Pod::Parser will be removed from 5.32; as a consequence,
libpod-parser-perl was uploaded as a separate package to Debian,
and some progress made towards migrating other packages to make
use of it.

Most of the known issues have been filed and can be seen at

<https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=perl-5.32-transition;users=debian-perl@lists.debian.org>

Hardening flags
---------------

The group agreed, following some discussion, to standardise on
using the hardening=+all option, rather than only
hardening=+bindnow. This change was reflected across pkg-perl git
repositories.

MUT (Multi Upstream Tarball)
----------------------------

Progress was made towards the goal of migrating away from obsolete
tools for packaging multiple tarballs; in particular, libwww-search-perl
was moved away from pkg-components.

Concerns about packaging `Alien::*`
---------------------------------

The group discussed the best way to introduce `Alien::*` packages,
which are becoming increasingly popular, into Debian in a way which
meets our user and developer needs: particularly how it its automatic
source code downloading behaviour should be configured.

It was agreed to prepare a sample package, patching it to default to
`install_method=system`, for review by the team.

Package maintenance
-------------------

Various routine package maintenance actions were taken, including
switching to debhelper 13, and uploading new upstream releases.

Website
-------

The team policy was revised, and the website build system improved.

NEWS.Developer
--------------

A discussion was had about the concern that API changes and other
similar developer news items may not be suitable for debian/NEWS
files, since the average user does not need to know about them. Conversely,
there would be some news items in perl modules that should be user-
visible), for example where system configurations need to be updated.
The current sitaution where debian/NEWS is a mix of user and developer
news serves neither users nor developers well.

It was proposed that we standardise on a `debian/NEWS.Developer` file, to
be installed in a similar manner to `dh_installchangelogs`.

The team policy was updated in a [merge request][]
to reflect this new practice. The intention is to bring this discussion
to the project as a whole, initially as a proposal to update the
functionality of apt-listchanges.

[merge request]: https://salsa.debian.org/perl-team/perl-team.pages.debian.net/-/merge_requests/8)

SSL verification by default
---------------------------

A proposal to enable SSL verification by default in HTTP::Tiny was
brought to the group for discussion. Upstream prefers to not
make this change, but the consensus amongst the group is that
we should flip the default to true if practical. The impact is
somewhat difficult to measure, but we will start by doing a test
rebuild of perl build-dependencies.
