Debian Perl Group Sprint @ DebCamp 2015
=======================================

Preparation
-----------
https://wiki.debian.org/Sprints/2015/DebianPerlDebCamp
https://wiki.debian.org/Teams/DebianPerlGroup/OpenTasks

Mon, 2015-08-10
---------------
Participants: gregoa, XTaran, carnil, fsfs, bremner, intrigeri
Topics: DUCK, lintian tests in pkg-perl-tools, libstdc++6 transition + Perl 5.22 Transition, libnet-xmpp-perl, old packages, repo cleanup, pinging people to push tags etc., updating perl packages which popped up on PET, gitpkg patches by Niko, libogre-perl removal
Report → see below

- Meetings in the morning for coordination (and reports)
- Document everything here in gobby

Tue, 2015-08-11
---------------
Participants: carnil (inside), bremner, fsfs, XTaran, gregoa, ansgar (git-debcherry afteroon)
Topics: lintian checks, gitpkg/git-debcherry (15:30), libdatetime-timezone-perl, libnet-xmpp-perl (new information from bug reporter)
Report → see below

Wed, 2015-08-12
---------------
Participants: carnil, fsfs, ansgar, bremner, gregoa
Topics: tidy up git-debcherry scripts after gitpkg upload (bremner); upload packages and try git-debcherry (fsfs), gbp-pull + notes bug (bremner), RC bugs/new upstream releases (gregor)
Report → see below

Thu, 2015-08-13
---------------
Participants: carnil, fsfs, bremner, kanashiro, XTaran, gregoa
Topics: bremner to work on an upstream bug that gregoa forwarded yesterday (and maybe the things he said yesterday); fsfs: try git-debcherry on a package and continue, axel: continue moving lintian checks from pkg-perl-tools to lintian proper; axel: run (new) DUCK again; axel: look at not-uploaded-for-N-years packages; tidy up git-debcherry scripts after gitpkg upload (bremner); new upstream releases (kanashiro)
Report → see below

Fri, 2015-08-14
---------------
Participants: bremner, fsfs, carnil, gregoa, XTaran
Topics: dh-dist-zilla, libdist-zilla-perl, etc. + finishing moving lintian tests mover (XTaran); git-debcherry experimentation (fsfs); perl5.22 (fsfs, carnil, gregoa)
Report → see below



REPORT
======


* git-debcherry improvements (especially notes)
#784130 ping maintainer, patch seems ready. [bremner] -> uploaded
#784159 Style updates to patch, sign-off on new feature [bremner] -> uploaded


QA tasks
--------

PET:
- missing tags: ping maintainers to push missing tags/branches to git.deban.org
- repo cleanup: ping maintainers about packages which are in git but never uploaded

Unification
- unify paths in lintian overrides for multi-arch paths in hardening-no-fortify-functions tag
- unify uversionmangle in d/watch: cancelled, too much variance, no clear "best" option, too little gain
- switch from repacking framework to Files-Excluded: cancelled, mk-origtargz errors out too often
- unify environment variables for disabling network tests (debian/rules and debian/patches)
- unify TEST_FILES filtering in debian/rules

pkg-perl-tools:
- update examples/check-build: run adt-run; manipulate .changes with mergechanges; add support for adt-qemu
- patchedit: Support $EDITOR containing options or parameters [abe/carnil]
- moved all checks which were planned to move over to lintian (feature branch ppt-lc-mover on both sides) [abe]
- import-orig: check also for upstream tags with padding zeros removed

Forwarding patches upstream:
- libglib-perl: cleaned up patches, split some, and forwarded them all upstream.

Packages
--------

individual packages:
* #732725 (libogre-perl FTBFS) → #795067 (RM; ROM) [abe]
* #794963 (Net::XMPP warnings under setuid/root): Tried to reproduce [abe]
* libdatetime-timezone-perl: update to olson db 2015f in sid, jessie, wheezy
* #791507: new upstream version [bremner], uploaded to debian [carnil]

all or many packages.
* Forward bugs reports upstream.
* Ping upstream bug reports.
* Report new bugs from going through the reproducible build logs.
* Update liburi-perl and libcatalyst-perl (fixes an RC bug).
* Update lots of packages to new upstream versions. [kanashiro]
* Go through ci.debian.net failures, fix packages, file bugs.

Statistics
----------
Andreas Tille: pkg-perl is the only team where most packages are maintained by 3 people
TODO: count number of uploads (IRC logs)

git & patches
-------------

Demo of git-debcherry and Niko's patches. (Uploaded by the end of the day.)
Works :)
Minor glitch in notes2dep3: blank lines
Minor glitch in git-debcherry From.*$hash at the top.
Missing feature in git-debcherry: access to original commit to see easily what changed against upstream code.

The question is still: How to work on a git-debcherried package without buying into git-debcherry?
Brainstorming: commit patches on a specific branch, allow people to work there, create tooling to import changes / new patches from this branch back into master (at `dpt checkout' time?). Maybe with a special header "X-Generated-By: git-debcherry" to filter?

If we had a patch branch:
- when/how to write to this branch? when: commit at sign/tag/upload time
- when/how to re-import into master manually created patches?

gbp doesn't fetch notes yet (bug report by XTaran)

+ fsfs and bremner will try on a real package.
+ bremner looks at the gbp patch.
