
=head1 Quilt for Debian Maintainers

This guide briefly introduces usage of C<quilt> for managing patches to
debian packages.

It will describe how to create, modify, apply and unapply patches with
C<quilt>.

This is a work in progress.  Please send any comments or ideas to
<jawnsy@cpan.org>.

=head2 Copyable Version

This is a version of the original document, which has been forked to
provide copy/paste-compatible information. The original B<quilt.pod> and
this version diverged in revision 40966.

Please consider B<quilt.pod> to be the authoritative version of this
document, maintained by Damyan Ivanov.

=head1 The Big Picture

C<quilt> helps managing a series of patches that a debian package maintainer
needs applied to upstream source when building the package. It is similar to
C<dpatch> and C<cdbs>'s simple-patchsys in that the patches are kept as a
series of files in F<debian/patches>.

=head2 Preparation

C<quilt> uses a special directory for keeping patches (not very surprising).
Unfortunatelly, that directory is F<./patches/> by default. For Debian packages
F<./debian/patches/> is far more comfortable. To flawlessly fix this, add
C<export QUILT_PATCHES=debian/patches> in your shell resource file and reload
it. This way you won't have to worry about this quilt oddity (like me; even
forgot to mention it in the first revision of this document).

You can also create F<.quiltrc> in your home directory and set
C<QUILT_PATCHES> there if you prefer.

=head2 Patch Order: the 'series'

C<quilt> manages the series of patches as a stack. You push to it (apply a
patch) and pop from it (unapply a patch).

There's one special file, F<debian/patches/series>, which contains the
list of all patches to be applied. C<quilt> applies patches in the order they
are listed in that file.

=head2 Philosophy

C<quilt> works in the source tree. There's no copying large trees around.
There's also no need to always keep the source tree clean of unapplied changes
just to be able to extract a diff with SVN.

To achieve this, however, C<quilt> needs to know which files are contained in
each patch. The workflow is as follows:

=over

=item 1. you tell C<quilt> that you start working on a given patch

=item 2. you work on that patch

=item 3. you tell C<quilt> that you're done

=back

At first this sounds complicated but it actually it is not. You just speak to
your patch system so it knows what you're doing and handles the details.

=head2 Working With quilt

Here are a few examples of working with C<quilt>. Note that most of the
commands below have plenty of options so reading quilt man page is a good idea.

=head2 Creating a Patch

To create a patch, run C<quilt new $patch_name>. If you want C<.patch>
extension, you need to give it.

This will create a new patch entry in F<debian/patches>. This patch also
becomes the I<topmost> or current patch. This is the patch that is at the top
of the stack.

Now choose what file you want to change in that patch and run
C<quilt edit $file>. C<quilt> will note the current state of the file and
launch your C<$EDITOR> so you can edit the file.

Repeat the C<quilt edit> command for all files you want to be changed by that
patch.

When you're finished, run C<quilt refresh>. This will compare the noted state
of the edited files with their present state and produce a patch in
F<debian/patches>.

Note that that patch is currently applied. Check it with C<quilt applied>.

If the package is already being maintained in the pkg-perl svn repository, it is
necessary to tell svn that you have added new files. You can do this with
C<svn add debian/patches>.

=head2 Applying and Unapplying Patches

Now that we have the patch applied, let's play with it. C<quilt pop> will
unapply the topmost patch. C<quilt push> applies the next patch in the series.
You may see the list of unapplied patches with C<quilt unapplied>.

=head2 Editing Patches

To edit a patch, you have to first make it current (be on the top of the stack
of applied patches). If the patch is already applied (but not the top), run
C<quilt pop $patch_name>; if it is not, run C<quilt push $patch_name>. Now that
the patch is on the top of the stack, run C<quilt edit $file> as before. You
can edit files that were already in the patch and you can edit new files. When
you're done, remember to tell this to C<quilt> by running C<quilt refresh>.

=head2 Other Commands

C<quilt delete> deletes, C<quilt rename> renames a patch. There are a lot
more. See the manual page.

=head1 Integrating With the Package Build Process

Obviously, C<quilt> needs to be added to C<Build-Depends>. Note that
C<Build-Depends-Indep> (for arch:all packages) is not appropriate as
C<quilt> is needed by the C<clean> target.

Note: Patches should be unapplied before committing to SVN! If you forget,
don't worry, lintian will issue a warning to remind you.

=head2 The "Classic" Way

C<include /usr/share/quilt/quilt.make> has some candy for F<debian/rules>.
First, it defines C<QUILT_PATCHES> to be F<debian/patches> so that one doesn't
have to worry about that setting during the build process.

Second, it defines C<QUILT_STAMPFN>, which can be used as a C<build-stamp> or
C<configure-stamp> dependency.

Lastly, C<unpatch> target is useful as a C<clean> dependency.

Here's a sample fragment from F<debian/rules>:

=begin html

<pre>include /usr/share/quilt/quilt.make

build: build-stamp
build-stamp: $(QUILT_STAMPFN)
&#09;dh_testdir
&#09;...
clean: unpatch
&#09;dh_clean build-stamp
&#09;...</pre>

=end html

Note that using the following:

=begin html

<pre>build: patch build-stamp
build-stamp:
&#09;...</pre>

=end html

can fail if parallel build is allowed via C<-jN>. C<patch> and C<build-stamp>
would be run in parallel, possibly failing due to partly applied patches.

N.B. F</usr/share/quilt/quilt.make> is available since C<quilt> version 0.40.
This version is available since Etch so the build-dependency can usually be
without version.

=head2 The "Modern" Way

Since 0.46-7 C<quilt> offers a simpler integration with debhelper. A simple
F<debian/rules> can now be written as (needs C<debhelper> >= 7.0.8):

=begin html

<pre>%:
&#09;dh $@ --with quilt</pre>

=end html

Alternatively C<dh_quilt_patch> and C<dh_quilt_unpatch> can be added to
F<debian/rules>.

For the details take a look at F</usr/share/doc/quilt/README.Debian> and/or
C<dh_quilt_patch(1)>, C<dh_quilt_unpatch(1)>.

=head2 The "Post-Modern" Way

Since patches are used so often, the "3.0 (quilt)" source package format
supports them by default. A running build system will use quilt if it's
available and C<dpkg-source> can apply patches otherwise. To test your
package, you can do this:

    $ apt-get source libacme-foo-perl
    $ mkdir -p libacme-foo-perl-0.1/debian/source
    $ echo "3.0 (quilt)" >libacme-foo-perl-0.1/debian/source/format
    $ dpkg-source -b libacme-foo-perl-0.1
    $ dpkg-source -x libacme-foo-perl-0.1-1.dsc
    $ cd libacme-foo-perl-0.1 && debuild -us -uc

These instructions come from: L<http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=538676>
-- you can find more details there.

=head3 Renaming Files

Sometimes you'll need to rename a file, for example if there are some nice
scripts you'd like to have installed, but they have a B<.pl> extension. For
Debian packages, scripts are usually installed with the extension dropped,
so that you can call it on the command line as:

    $ some_utility blahblah
    # rather than
    $ some_utility.pl blahblah

In this case, you will need to patch the script file itself so that it only
refers to itself as the new name. Afterward, you rename the file using an
override (see the the C<debhelper> article for more). You need to make sure
the patch is applied first, then the file can be renamed -- this way, if
one uses C<dpkg-source> to do the patching, it will work properly -- if
you rename the file first, then dpkg-source won't be able to find the file
(since the build isn't being run, and the file hasn't been renamed yet)
and it will fail.

=begin html

<pre>%:
&#09;dh $@ --with quilt
override_dh_quilt_patch:
&#09;dh_quilt_patch
&#09;[ -f bin/some_utility ] || mv bin/some_utility.pl bin/some_utility
override_dh_quilt_unpatch:
&#09;[ -f bin/some_utility.pl ] || mv bin/some_utility bin/some_utility.pl
&#09;dh_quilt_unpatch
</pre>

=end html

=head2 README.source

As of Debian Policy version 3.8.0, packages that use a patch system, such as
quilt, are required to have a F<debian/README.source> file explaining how to
generate the patched source, add a new modification, and remove an existing
modification. A standard F<debian/README.source> file is used for all packages
maintained by the Debian Perl Group. The F<debian/README.source> file looks
like this:

=begin html

<pre>This package uses quilt to manage all modifications to the upstream
source.  Changes are stored in the source package as diffs in
debian/patches and applied during the build.
&nbsp;
See /usr/share/doc/quilt/README.source for a detailed explanation.</pre>

=end html

You can also use C<packagecheck -Q -c> to generate the F<debian/README.source>
file.

=head1 Why quilt (versus other patch systems)

=over

=item * patch creating/editing is faster

This is due to the fact that quilt works within the source tree. C<dpatch>
needs to create a copy, then diff recursively.

Using C<svn> to create patches is impossible if you have a file that is modified
by several patches. C<quilt> does this naturally.

=item * fixing a rejected file is easier

    quilt push -f # force rejections
    quilt edit files-with-rejections
    quilt refresh

=item * refreshing a fuzzy patch is a breeze

    quilt push
    quilt refresh

=back

=head1 Tips and Tricks

=head2 Cleaner Patches

By default, C<quilt> saves patches that look like the output from C<diff>.
This means they include timestamps for each changed file.  C<quilt> also
preserves C<Index:> lines, such as those generated by C<svn diff>.  However,
C<patch> doesn't need any of that data to apply the patch and it has the
unfortunate side effect of causing every file header line of the patch to
change each time someone runs C<quilt refresh>.

To use the simplest headers in the patch, add:

    QUILT_DIFF_ARGS="--no-timestamps --no-index -pab"
    QUILT_REFRESH_ARGS="--no-timestamps --no-index -pab"

to F<~/.quiltrc> or set those variables in the environment if you prefer.  The
next time you C<quilt refresh> a patch, C<quilt> will remove the extraneous
information.

=head2 Unified Reject Files

If upstream has made a change that conflicts with a patch, one normally
force-applies the patch (with C<quilt push -f I<patch>>) and then looks at the
resulting F<*.rej> file to see what part of the patch failed.  By default, the
F<*.rej> file is a traditional context diff similar to the output of C<diff
-c>.  If you're used to the output of C<diff -u>, you can tell C<quilt> to
create unified context diffs for the F<*.rej> files by putting:

    QUILT_PATCH_OPTS="--unified-reject-files"

in your F<~/.quiltrc> or by setting that variable in the environment.

B<Attention>: I<--unified-reject-files> was removed in C<patch 2.6.1-1>.
Having this option in F<~/.quiltrc> breaks quilt.

=head1 Version

$Id: quilt.pod 40966 2009-07-29 02:40:27Z jawnsy-guest $

=head1 Authors

=over

=item * Damyan Ivanov <dmn@debian.org>

=back

=head1 Contributors

=over

=item * gregor herrmann <gregoa@debian.org>

=item * Russ Allbery <rra@debian.org>

=item * Jonathan Yu <jawnsy@cpan.org>

=back

=head1 License and Copyright

Copyright (c) 2007-2011 by the individual authors and contributors noted
above.  All rights reserved. This document is free software; you may
redistribute it and/or modify it under the same terms as Perl itself

Perl is distributed under your choice of the GNU General Public License or the
Artistic License.  On Debian GNU/Linux systems, the complete text of the GNU
General Public License can be found in `/usr/share/common-licenses/GPL' and the
Artistic License in `/usr/share/common-licenses/Artistic'.

=for comment
vim:tw=79
