
=head1 HOW TO GET A CPAN MODULE INTO DEBIAN.

This guide gives an overview of filing an RFP bug to get a CPAN
module into debian.

This is a work in progress.  Please send any comments or ideas to
<jeremiah@jeremiahfoster.com>.

=head2 0. Check the PTS

Perhaps the first thing to do when you want a specific perl module
brought to debian is to file a bug. There are at least two types of 
bugs you can file but the most likely one is the RFP bug; Request For
Packaging. You may want to check that the software is not already in 
debian by looking the at the PTS; Package Tracking System. After a
quick check of the PTS determines that the module you want packaged
is not being worked on or already in debian, go ahead and file the
RFP bug. 

The RFP bug is easily filed and here is an example of what one looks
like as an email, which is often the easiest way of interacting with
debian BTS (Bug Tracking Service.) Debian has a tool to help you with
this called `reportbug` so if you are on a debian system you can use
reportbug to create the RFP bug for your module. Here I'll go through
the steps of reporting a bug and describe the finished bug report.

=head2 1. reportbug

The first step is to install `reportbug` if you are on a debian system,
use aptitude to install if you do not have reportbug availble. Once
it is installed we'll call it like this:

reportbug --email you@your-email.com wnpp 

Using reportbug this way automatically tells the BTS that we are
reporting a bug on wnpp (Work-Needing and Prospective Packages). wnpp
is a "pseudo package" which we use because we cannot report a bug
against software that is not yet in debian. Obviously you would
replace the fake email address with your own.

reportbug runs and then presents you with a dialog asking you to
specify the type of bug. We are filing an RFP bug and that is
number 5. (Note that this menu of choices only comes up if you are
filing a bug against wnpp, if you are filing a bug against something
else, your menu will differ.)

=head2 2. Picking a name

After you hit 5 and enter, reportbug will prompt you to enter a name
for the proposed software, this will be the name of the CPAN module
that debian uses internally. The perl packaging policy is fairly
explicit on the naming convention debian uses. Following this policy
is a good idea because once you get used to it you can find a perl
module on Debian simply by knowing its CPAN name. The policy can be
paraphrased as prepending the prefix 'lib' to the module name,
replacing "::" with "-" wherever it occures in the name, and adding
"-perl" to the end and making sure the entire name is lowercase. So
a CPAN module with the name of Test::Dependencies would become 
libtest-dependecies-perl. 

Once you have entered a name for the CPAN module, reportbug asks for
a short description. reportbug then queries the BTS to find other
bugs which may be related to this bug. If you see your package
already listed as a bug that may mean that someone else asked for it
to be packaged or that someone is working on it. In either case, you
probably do not need to submit another report. If you see nothing
relevant to your package simply continue with your bug report. 

=head2 3. Finished bug report

Finally an RFP bug report will come up in an editor where you can fill
in necessary details so that the CPAN module can be package for
debian. If you want you can CC the debian-perl group who maintain
CPAN modules for debian with this line somewhere in the header of the
email:

X-Debbugs-CC: debian-perl@lists.debian.org

Once your RFP bug is filed, you will get an acknowledgement of your
bug report to the email address you submitted and a bug number will
be assigned with which you can track the progress of the module as it
makes its way into debian. Once the finished package is complete that
bug should get automatically closed and you should get a report
saying so.

=head1 AUTHORS

=over

=item * jeremiah foster <jeremiah@cpan.org>

=back

=head1 License and Copyright

Copyright (c) 2007-2008 by the individual authors and contributors noted
above.  All rights reserved. This document is free software; you may
redistribute it and/or modify it under the same terms as Perl itself

Perl is distributed under your choice of the GNU General Public License or the
Artistic License.  On Debian GNU/Linux systems, the complete text of the GNU
General Public License can be found in `/usr/share/common-licenses/GPL' and the
Artistic License in `/usr/share/common-licenses/Artistic'.
